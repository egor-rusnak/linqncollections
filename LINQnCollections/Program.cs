﻿using LINQnCollections.Application.Commands;
using LINQnCollections.Domain.Services;
using LINQnCollections.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LINQnCollections
{
    class Program
    {
        private static List<ICommand> commands = new List<ICommand>
        {
            new ExecuteFirstQuery(),
            new ExecuteSecondQuery(),
            new ExecuteThirdQuery(),
            new ExecuteFourthQuery(),
            new ExecuteFifthQuery(),
            new ExecuteSixthQuery(),
            new ExecuteSeventhQuery()
        };

        static async Task Main(string[] args)
        {
            HttpProjectDataService provider = new HttpProjectDataService(new Uri("https://bsa21.azurewebsites.net"));

            var projectService = new ProjectService(provider);
            await projectService.GetDataFromProvider();

            RunApplication(projectService);
        }

        static void RunApplication(ProjectService service)
        {
            Console.WriteLine("Console interface for BS task `Linq and Collections`");
            Console.WriteLine("To show menu, write a 0");
            ShowMenu();
            while (true)
            {
                Console.Write("Input a command: ");
                var input = Console.ReadLine();

                if (int.TryParse(input, out int command))
                {
                    if (command > 0 && command < 8)
                    {
                        commands[command - 1].Execute(service);
                        Console.WriteLine();
                        continue;
                    }
                    else if (command == 0)
                    {
                        ShowMenu();
                        continue;
                    }
                    else if (command == 8) break;
                }

                Console.WriteLine("Wrong Input");
            }
            Console.WriteLine("Good Bye!");
        }

        static void ShowMenu()
        {
            Console.WriteLine("0-Show menu");
            Console.WriteLine("1-Execute 1 query");
            Console.WriteLine("2-Execute 2 query");
            Console.WriteLine("3-Execute 3 query");
            Console.WriteLine("4-Execute 4 query");
            Console.WriteLine("5-Execute 5 query");
            Console.WriteLine("6-Execute 6 query");
            Console.WriteLine("7-Execute 7 query");
            Console.WriteLine("8-Exit");
        }
    }
}
