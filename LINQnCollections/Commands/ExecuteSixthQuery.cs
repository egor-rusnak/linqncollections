﻿using LINQnCollections.Domain.Services;
using System;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteSixthQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Result for user {input}: ");
                var result = projectService.ExecuteQuerySix(input);
                if (result != null)
                {
                    Console.WriteLine($"[{result.User}]");
                    Console.WriteLine($"Last Project by Created date: {result.LastProject.Id} {result.LastProject.Name}");
                    Console.WriteLine($"Count of tasks in last proejct: {result.LastProjectTasksCount}");
                    Console.WriteLine($"Unfinished tasks: {result.UnfinishedTasksCount}");
                    Console.WriteLine("Longest task (if finished is null than using now): " + (result.LongestTask?.ToString() ?? "No tasks!"));
                }
                else Console.WriteLine("BLANK(No user in Data)");
            }
        }
    }
}
