﻿using LINQnCollections.Domain.Services;
using System;
using System.Linq;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteFourthQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.WriteLine("Teams with users older than 10 years: ");
            var result = projectService.ExecuteQueryFour();
            foreach (var elem in result)
            {
                Console.WriteLine($"Team [{elem.Key.Key} - {elem.Key.Value}] (ordered DESC by register date):");
                foreach (var user in elem)
                {
                    Console.WriteLine("\t" + user + " Registered:  " + user.RegisteredAt + " BirthDay: " + user.BirthDay);
                }
                if (elem.Count() == 0) Console.WriteLine("\tBLANK");

                Console.WriteLine("===============");
            }
            if (result.Count() == 0) Console.WriteLine("BLANK");
        }
    }
}
