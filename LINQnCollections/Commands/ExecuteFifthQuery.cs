﻿using LINQnCollections.Domain.Services;
using System;
using System.Linq;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteFifthQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.WriteLine("Users ordered by first name: ");
            var result = projectService.ExecuteQueryFive();
            foreach (var elem in result)
            {
                Console.WriteLine($"User [{elem.Key}] with sorted by name length tasks:");
                foreach (var task in elem.Value)
                {
                    Console.WriteLine("\t" + task);
                }
                if (elem.Value.Count() == 0) Console.WriteLine("\tBLANK");
            }
            if (result.Count() == 0) Console.WriteLine("BLANK");
        }
    }
}
