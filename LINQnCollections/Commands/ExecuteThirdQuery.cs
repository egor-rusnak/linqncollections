﻿using LINQnCollections.Domain.Services;
using System;
using System.Linq;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteThirdQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Finished Tasks in {DateTime.Now.Year} from user {input}: ");
                var result = projectService.ExecuteQueryThree(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"Task id: {elem.Key}, Name: {elem.Value}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }
        }
    }
}
