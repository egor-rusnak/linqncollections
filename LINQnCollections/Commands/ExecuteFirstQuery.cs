﻿using LINQnCollections.Domain.Services;
using System;
using System.Linq;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteFirstQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine("Result: ");
                var result = projectService.ExecuteQueryOne(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"Project id: {elem.Key.Id} and tasks count: {elem.Value}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }
        }
    }
}
