﻿using LINQnCollections.Domain.Services;
using System;
using System.Linq;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteSeventhQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.WriteLine($"Result: ");
            var result = projectService.ExecuteQuerySeven();
            foreach (var elem in result)
            {
                Console.WriteLine($"Project [{elem.Project.Id} - {elem.Project.Name}]");
                Console.WriteLine($"Longest Task by date: [{elem.LongestTask}]");
                Console.WriteLine($"Shortest Task by name: [{elem.ShortestTask}]");
                Console.WriteLine($"Users count with project description > 20 length or tasks count <3: {elem.UsersCountInTeam?.ToString() ?? "Not match conditions"}");
                Console.WriteLine("=========================");
            }
            if (result.Count() == 0) Console.WriteLine("BLANK");
        }
    }
}
