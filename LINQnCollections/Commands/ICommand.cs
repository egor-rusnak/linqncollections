﻿using LINQnCollections.Domain.Services;

namespace LINQnCollections.Application.Commands
{
    public interface ICommand
    {
        public void Execute(ProjectService projectService);
    }
}
