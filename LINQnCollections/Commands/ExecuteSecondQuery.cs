﻿using LINQnCollections.Domain.Services;
using System;
using System.Linq;

namespace LINQnCollections.Application.Commands
{
    public class ExecuteSecondQuery : ICommand
    {
        public void Execute(ProjectService projectService)
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Tasks for user [{input}] where task name length < 45: ");
                var result = projectService.ExecuteQueryTwo(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"{elem}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }

        }
    }
}
