﻿using LINQnCollections.Domain.DTOs;
using LINQnCollections.Domain.Entities;
using LINQnCollections.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace LINQnCollections.Services
{
    public class HttpProjectDataService : IProjectDataProvider
    {
        private readonly HttpClient _client;
        public HttpProjectDataService(Uri apiBaseUrl)
        {
            _client = new HttpClient();
            _client.BaseAddress = apiBaseUrl;
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            var projects = await GetProjectsDtos();
            var tasks = await GetTasksDtos();
            var teams = await GetTeamsDtos();
            var users = await GetUsersDtos();

            var projectEntities = projects
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, t) => new { Project = p, Tasks = t })
                .Join(teams, p => p.Project.TeamId, t => t.Id, (p, t) => new { p.Project, Team = new Team(t.Id, t.Name, t.CreatedAt), p.Tasks })
                .Select(p =>
                {
                    var usersEntities = users.Select(u => new User(u.Id, u.FirstName, u.LastName, u.Email, u.RegisteredAt, u.BirthDay, u.TeamId));
                    return new Project(
                        p.Project.Id,
                        p.Project.Name,
                        p.Project.Description,
                        p.Tasks.Select(t => new Domain.Entities.Task(t.Id, t.Name, usersEntities.FirstOrDefault(u => u.Id == t.PerformerId), t.ProjectId, t.Description, t.CreatedAt, t.FinishedAt)),
                        usersEntities.FirstOrDefault(u => u.Id == p.Project.AuthorId),
                        p.Team,
                        p.Project.Deadline,
                        p.Project.CreatedAt
                        );
                });
            return projectEntities;
        }

        private async Task<IEnumerable<ProjectDto>> GetProjectsDtos()
        {
            return await _client.GetFromJsonAsync<IEnumerable<ProjectDto>>("api/projects");
        }
        private async Task<IEnumerable<TaskDto>> GetTasksDtos()
        {
            return await _client.GetFromJsonAsync<IEnumerable<TaskDto>>("api/tasks");
        }
        private async Task<IEnumerable<TeamDto>> GetTeamsDtos()
        {
            return await _client.GetFromJsonAsync<IEnumerable<TeamDto>>("api/tasks");
        }
        private async Task<IEnumerable<UserDto>> GetUsersDtos()
        {
            return await _client.GetFromJsonAsync<IEnumerable<UserDto>>("api/users");
        }
    }
}
