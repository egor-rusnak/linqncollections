﻿using LINQnCollections.Domain.Entities;

namespace LINQnCollections.Domain.QueryResults
{

    //for query 6
    public class UserInfo
    {
        public User User { get; private set; }
        public Project LastProject { get; private set; }
        public int LastProjectTasksCount { get; private set; }
        public int UnfinishedTasksCount { get; private set; }
        public Task LongestTask { get; private set; }

        public UserInfo(User user, Project lastProject, int lastProjectTasksCount, int canceledAndUnfinishedTasksCount, Entities.Task longestTask)
        {
            User = user;
            LastProject = lastProject;
            LastProjectTasksCount = lastProjectTasksCount;
            UnfinishedTasksCount = canceledAndUnfinishedTasksCount;
            LongestTask = longestTask;
        }
    }
}
