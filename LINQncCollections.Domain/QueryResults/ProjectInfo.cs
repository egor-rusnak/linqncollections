﻿using LINQnCollections.Domain.Entities;

namespace LINQnCollections.Domain.QueryResults
{
    //for query 7
    public class ProjectInfo
    {
        public Project Project { get; private set; }
        public Task LongestTask { get; private set; }
        public Task ShortestTask { get; private set; }
        public int? UsersCountInTeam { get; private set; }

        public ProjectInfo(Project project, Task longestTask, Task shortestTask, int? usersCountInTeam)
        {
            Project = project;
            LongestTask = longestTask;
            ShortestTask = shortestTask;
            UsersCountInTeam = usersCountInTeam;
        }
    }
}
