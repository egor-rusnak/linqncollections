﻿using LINQnCollections.Domain.DTOs.Abstraction;
using System;

namespace LINQnCollections.Domain.DTOs
{
    public class TeamDto : BaseEntityDto
    {
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }
    }
}
