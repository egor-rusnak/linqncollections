﻿using LINQnCollections.Domain.DTOs.Abstraction;
using System;

namespace LINQnCollections.Domain.DTOs
{
    public class TaskDto : BaseEntityDto
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public int PerformerId { get; set; }
    }
}
