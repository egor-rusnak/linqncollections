﻿using LINQnCollections.Domain.DTOs.Abstraction;
using System;

namespace LINQnCollections.Domain.DTOs
{
    public class ProjectDto : BaseEntityDto
    {
        public string Name { get; set; }
        public int AuthorId { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public int TeamId { get; set; }
    }
}
