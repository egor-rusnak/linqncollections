﻿using LINQnCollections.Domain.DTOs.Abstraction;
using System;

namespace LINQnCollections.Domain.DTOs
{
    public class UserDto : BaseEntityDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAt { get; set; }
        public string Email { get; set; }
        public int? TeamId { get; set; }
    }
}
