﻿namespace LINQnCollections.Domain.DTOs.Abstraction
{
    public abstract class BaseEntityDto
    {
        public int Id { get; set; }
    }
}
