﻿using LINQnCollections.Domain.Entities.Abstraction;
using System;

namespace LINQnCollections.Domain.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public Team(int id, string name, DateTime createdAt) : base(id)
        {
            Name = name;
            CreatedAt = createdAt;
        }
    }
}
