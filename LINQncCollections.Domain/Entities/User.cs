﻿using LINQnCollections.Domain.Entities.Abstraction;
using System;

namespace LINQnCollections.Domain.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public int? TeamId { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime registeredAt, DateTime birthDay, int? teamId) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            RegisteredAt = registeredAt;
            BirthDay = birthDay;
            TeamId = teamId;
        }

        public override string ToString()
        {
            return Id + "-" + FirstName + " " + LastName;
        }
    }
}
