﻿using LINQnCollections.Domain.Entities.Abstraction;
using System;
using System.Collections.Generic;

namespace LINQnCollections.Domain.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public string Description { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public Project(int id, string name, string description, IEnumerable<Task> tasks, User author, Team team, DateTime deadline, DateTime createdAt) : base(id)
        {
            Description = description;
            Name = name;
            Tasks = tasks ?? new List<Task>();
            Author = author;
            Team = team;
            Deadline = deadline;
            CreatedAt = createdAt;
        }
    }
}
