﻿using LINQnCollections.Domain.Entities.Abstraction;
using System;

namespace LINQnCollections.Domain.Entities
{
    public class Task : BaseEntity
    {
        public string Name { get; set; }
        public User Performer { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public Task(int id, string name, User performer, int projectId, string description, DateTime createdAt, DateTime? finishedAt) : base(id)
        {
            Performer = performer;
            ProjectId = projectId;
            Description = description;
            CreatedAt = createdAt;
            FinishedAt = finishedAt;
            Name = name;
        }

        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
