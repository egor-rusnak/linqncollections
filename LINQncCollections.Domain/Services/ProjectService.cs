﻿using LINQnCollections.Domain.Entities;
using LINQnCollections.Domain.Entities.Comparers;
using LINQnCollections.Domain.Interfaces;
using LINQnCollections.Domain.QueryResults;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQnCollections.Domain.Services
{
    public class ProjectService
    {
        private readonly IProjectDataProvider _dataProvider;
        private IEnumerable<Project> projects;

        public ProjectService(IProjectDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public async System.Threading.Tasks.Task GetDataFromProvider()
        {
            projects = await _dataProvider.GetProjects();
        }

        //In Dictionary value - count
        public Dictionary<Project, int> ExecuteQueryOne(int userId)
        {
            return new Dictionary<Project, int>(projects
                .SelectMany(p => p.Tasks)
                .Where(t => t.Performer.Id == userId)
                .GroupBy(t => t.ProjectId)
                .Join(projects, groupTasks => groupTasks.Key, p => p.Id, (groupTasks, p) =>
                      new KeyValuePair<Project, int>(p, groupTasks.Count()))) ?? new Dictionary<Project, int>();
        }

        public IEnumerable<Task> ExecuteQueryTwo(int userId)
        {
            var result = projects.SelectMany(p => p.Tasks?.Where(t => t.Performer.Id == userId && t.Name.Length < 45));
            return result.ToList();
        }

        // In key - Id, value - name
        public IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId)
        {
            return projects
                .SelectMany(p => p.Tasks
                    .Where(t =>
                        t.Performer.Id == userId
                        && t.FinishedAt.HasValue
                        && t.FinishedAt.Value.Year == DateTime.Now.Year
                    ).Select(t => new KeyValuePair<int, string>(t.Id, t.Name))
                ).ToList();
        }

        //In key - Id, value - name, User - UsersList
        public IEnumerable<IGrouping<KeyValuePair<int, string>, User>> ExecuteQueryFour()
        {
            return projects.Select(p => p.Team)
                .Distinct(new TeamsEqualityComparer())
                .Join(
                    projects.Select(p => p.Author)
                        .Union(projects.SelectMany(p => p.Tasks.Select(t => t.Performer)))
                        .Distinct(new UsersEqualityComparer())
                        .OrderByDescending(u => u.RegisteredAt),
                    t => t.Id, u => u.TeamId, (t, u) => new { Team = t, User = u }
                )
                .GroupBy(t => new KeyValuePair<int, string>(t.Team.Id, t.Team.Name), e => e.User)
                .Where(t => !t.Any(u => u.BirthDay.Year >= DateTime.Now.Year - 10))
                .ToList();
        }

        public IEnumerable<KeyValuePair<User, IEnumerable<Task>>> ExecuteQueryFive()
        {
            return projects.SelectMany(p => p.Tasks.Select(t => t.Performer)
                .Union(projects.Select(m => m.Author)))
                .Distinct(new UsersEqualityComparer())
                .GroupJoin(
                    projects.SelectMany(p => p.Tasks),
                        u => u.Id,
                        t => t.Performer.Id,
                        (u, t) => new KeyValuePair<User, IEnumerable<Task>>(u, t.OrderByDescending(tsk => tsk.Name.Length)))
                .OrderBy(u => u.Key.FirstName)
                .ToList();
        }

        public UserInfo ExecuteQuerySix(int userId)
        {
            return projects.SelectMany(p => p.Tasks.Select(t => t.Performer))
                .Union(projects.Select(p => p.Author))
                .Distinct(new UsersEqualityComparer())
                .GroupJoin
                (
                    projects,
                    u => u.TeamId,
                    p => p.Team.Id,
                    (u, p) =>
                      new { User = u, LastProject = p?.OrderByDescending(e => e.CreatedAt).First() }
                )
                .GroupJoin(
                    projects.SelectMany(p => p.Tasks),
                    u => u.User.Id,
                    t => t.Performer.Id,
                    (u, t) => new { u.User, u.LastProject, Tasks = t }
                )
                .Select(e => new UserInfo(
                    e.User,
                    e.LastProject,
                    e.LastProject.Tasks.Count(),
                    e.Tasks?.Count(t => !t.FinishedAt.HasValue) ?? 0,
                    e.Tasks?.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)?.FirstOrDefault())
                )
                .FirstOrDefault(r => r.User.Id == userId);
        }

        public IEnumerable<ProjectInfo> ExecuteQuerySeven()
        {
            return
                projects.Select(p => new ProjectInfo(
                    p,
                    p.Tasks.OrderByDescending(t => t.Description.Length)?.FirstOrDefault(),
                    p.Tasks.OrderBy(t => t.Name.Length)?.FirstOrDefault(),
                    p.Description.Length > 20 || p.Tasks.Count() < 3 ?
                        projects.Select(pr => pr.Author)
                        .Union(projects.SelectMany(pr => pr.Tasks.Select(t => t.Performer)))
                        .Distinct(new UsersEqualityComparer())
                        .Where(u => u.TeamId == p.Team.Id)?.Count() ?? null : null)
                ).ToList();
        }
    }
}
