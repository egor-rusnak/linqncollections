﻿using LINQnCollections.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LINQnCollections.Domain.Interfaces
{
    public interface IProjectDataProvider : IDisposable
    {
        public Task<IEnumerable<Project>> GetProjects();
    }
}